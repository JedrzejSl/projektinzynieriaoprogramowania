﻿namespace FirmaOchroniarska
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lab_witaj = new System.Windows.Forms.Label();
            this.btnRaport = new System.Windows.Forms.Button();
            this.lab_menu = new System.Windows.Forms.Label();
            this.btnGrafik = new System.Windows.Forms.Button();
            this.btnNotatka = new System.Windows.Forms.Button();
            this.btnPracownicy = new System.Windows.Forms.Button();
            this.btnZespoly = new System.Windows.Forms.Button();
            this.labHome = new System.Windows.Forms.Label();
            this.lab_opcje = new System.Windows.Forms.Label();
            this.labPom = new System.Windows.Forms.Label();
            this.labWylog = new System.Windows.Forms.Label();
            this.pic_ochron = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_ochron)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 24.25F, System.Drawing.FontStyle.Bold);
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(5, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(315, 38);
            this.label3.TabIndex = 22;
            this.label3.Text = "Firma ochroniarska";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(24, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(277, 20);
            this.label1.TabIndex = 23;
            this.label1.Text = "Wszędzie i zawsze bezpiecznie";
            // 
            // lab_witaj
            // 
            this.lab_witaj.AutoSize = true;
            this.lab_witaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lab_witaj.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lab_witaj.Location = new System.Drawing.Point(25, 93);
            this.lab_witaj.Name = "lab_witaj";
            this.lab_witaj.Size = new System.Drawing.Size(0, 16);
            this.lab_witaj.TabIndex = 24;
            // 
            // btnRaport
            // 
            this.btnRaport.AllowDrop = true;
            this.btnRaport.BackColor = System.Drawing.Color.GhostWhite;
            this.btnRaport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRaport.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnRaport.FlatAppearance.BorderSize = 2;
            this.btnRaport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRaport.Location = new System.Drawing.Point(41, 175);
            this.btnRaport.Name = "btnRaport";
            this.btnRaport.Size = new System.Drawing.Size(177, 37);
            this.btnRaport.TabIndex = 25;
            this.btnRaport.Text = "Raport";
            this.btnRaport.UseVisualStyleBackColor = false;
            this.btnRaport.Click += new System.EventHandler(this.btnRaport_Click);
            // 
            // lab_menu
            // 
            this.lab_menu.AutoSize = true;
            this.lab_menu.BackColor = System.Drawing.Color.Transparent;
            this.lab_menu.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold);
            this.lab_menu.Location = new System.Drawing.Point(90, 143);
            this.lab_menu.Name = "lab_menu";
            this.lab_menu.Size = new System.Drawing.Size(88, 29);
            this.lab_menu.TabIndex = 26;
            this.lab_menu.Text = "MENU";
            // 
            // btnGrafik
            // 
            this.btnGrafik.BackColor = System.Drawing.Color.GhostWhite;
            this.btnGrafik.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGrafik.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnGrafik.FlatAppearance.BorderSize = 2;
            this.btnGrafik.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGrafik.Location = new System.Drawing.Point(41, 218);
            this.btnGrafik.Name = "btnGrafik";
            this.btnGrafik.Size = new System.Drawing.Size(177, 37);
            this.btnGrafik.TabIndex = 27;
            this.btnGrafik.Text = "Grafik";
            this.btnGrafik.UseVisualStyleBackColor = false;
            this.btnGrafik.Click += new System.EventHandler(this.btnGrafik_Click);
            // 
            // btnNotatka
            // 
            this.btnNotatka.BackColor = System.Drawing.Color.GhostWhite;
            this.btnNotatka.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNotatka.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnNotatka.FlatAppearance.BorderSize = 2;
            this.btnNotatka.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotatka.Location = new System.Drawing.Point(41, 261);
            this.btnNotatka.Name = "btnNotatka";
            this.btnNotatka.Size = new System.Drawing.Size(177, 37);
            this.btnNotatka.TabIndex = 29;
            this.btnNotatka.Text = "Notatka";
            this.btnNotatka.UseVisualStyleBackColor = false;
            this.btnNotatka.Click += new System.EventHandler(this.btnNotatka_Click);
            // 
            // btnPracownicy
            // 
            this.btnPracownicy.BackColor = System.Drawing.Color.GhostWhite;
            this.btnPracownicy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPracownicy.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnPracownicy.FlatAppearance.BorderSize = 2;
            this.btnPracownicy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPracownicy.Location = new System.Drawing.Point(41, 347);
            this.btnPracownicy.Name = "btnPracownicy";
            this.btnPracownicy.Size = new System.Drawing.Size(177, 37);
            this.btnPracownicy.TabIndex = 30;
            this.btnPracownicy.Text = "Pracownicy";
            this.btnPracownicy.UseVisualStyleBackColor = false;
            this.btnPracownicy.Click += new System.EventHandler(this.btnPracownicy_Click);
            // 
            // btnZespoly
            // 
            this.btnZespoly.BackColor = System.Drawing.Color.GhostWhite;
            this.btnZespoly.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnZespoly.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnZespoly.FlatAppearance.BorderSize = 2;
            this.btnZespoly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnZespoly.Location = new System.Drawing.Point(41, 304);
            this.btnZespoly.Name = "btnZespoly";
            this.btnZespoly.Size = new System.Drawing.Size(177, 37);
            this.btnZespoly.TabIndex = 31;
            this.btnZespoly.Text = "Zespoły";
            this.btnZespoly.UseVisualStyleBackColor = false;
            this.btnZespoly.Click += new System.EventHandler(this.btnZespoly_Click);
            // 
            // labHome
            // 
            this.labHome.AutoSize = true;
            this.labHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labHome.Location = new System.Drawing.Point(343, 15);
            this.labHome.Name = "labHome";
            this.labHome.Size = new System.Drawing.Size(62, 32);
            this.labHome.TabIndex = 33;
            this.labHome.Text = "Strona \r\nGłówna";
            // 
            // lab_opcje
            // 
            this.lab_opcje.AutoSize = true;
            this.lab_opcje.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lab_opcje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lab_opcje.Location = new System.Drawing.Point(411, 15);
            this.lab_opcje.Name = "lab_opcje";
            this.lab_opcje.Size = new System.Drawing.Size(94, 32);
            this.lab_opcje.TabIndex = 34;
            this.lab_opcje.Text = "Opcje \r\nUżytkownika";
            this.lab_opcje.Click += new System.EventHandler(this.lab_opcje_Click);
            // 
            // labPom
            // 
            this.labPom.AutoSize = true;
            this.labPom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labPom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labPom.Location = new System.Drawing.Point(511, 15);
            this.labPom.Name = "labPom";
            this.labPom.Size = new System.Drawing.Size(56, 16);
            this.labPom.TabIndex = 35;
            this.labPom.Text = "Pomoc";
            // 
            // labWylog
            // 
            this.labWylog.AutoSize = true;
            this.labWylog.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labWylog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labWylog.Location = new System.Drawing.Point(573, 15);
            this.labWylog.Name = "labWylog";
            this.labWylog.Size = new System.Drawing.Size(64, 16);
            this.labWylog.TabIndex = 36;
            this.labWylog.Text = "Wyloguj";
            this.labWylog.Click += new System.EventHandler(this.labWylog_Click);
            // 
            // pic_ochron
            // 
            this.pic_ochron.ErrorImage = null;
            this.pic_ochron.Image = global::FirmaOchroniarska.Properties.Resources.ochroniarz;
            this.pic_ochron.InitialImage = null;
            this.pic_ochron.Location = new System.Drawing.Point(301, 187);
            this.pic_ochron.Name = "pic_ochron";
            this.pic_ochron.Size = new System.Drawing.Size(266, 180);
            this.pic_ochron.TabIndex = 32;
            this.pic_ochron.TabStop = false;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(649, 413);
            this.Controls.Add(this.labWylog);
            this.Controls.Add(this.labPom);
            this.Controls.Add(this.lab_opcje);
            this.Controls.Add(this.labHome);
            this.Controls.Add(this.pic_ochron);
            this.Controls.Add(this.btnZespoly);
            this.Controls.Add(this.btnPracownicy);
            this.Controls.Add(this.btnNotatka);
            this.Controls.Add(this.btnGrafik);
            this.Controls.Add(this.lab_menu);
            this.Controls.Add(this.btnRaport);
            this.Controls.Add(this.lab_witaj);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Name = "Home";
            this.Text = "Home";
            this.Load += new System.EventHandler(this.Home_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.pic_ochron)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lab_witaj;
        private System.Windows.Forms.Button btnRaport;
        private System.Windows.Forms.Label lab_menu;
        private System.Windows.Forms.Button btnGrafik;
        private System.Windows.Forms.Button btnNotatka;
        private System.Windows.Forms.Button btnPracownicy;
        private System.Windows.Forms.Button btnZespoly;
        private System.Windows.Forms.PictureBox pic_ochron;
        private System.Windows.Forms.Label labHome;
        private System.Windows.Forms.Label lab_opcje;
        private System.Windows.Forms.Label labPom;
        private System.Windows.Forms.Label labWylog;
    }
}