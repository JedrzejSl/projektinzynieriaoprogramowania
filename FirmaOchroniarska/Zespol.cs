﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirmaOchroniarska
{
    public partial class Zespol : Form
    {
        string id;
        string zespolu_id;

        public Zespol(string id_z_home)
        {
            InitializeComponent();
            this.id = id_z_home;
            Load += new EventHandler(Zespol_load);
            label1.Visible = true;
            label2.Visible = true;
            label10.Visible = true;
            this.zespolu_id = "-1";
        }
        private void odswierz()
        {
            BazaDanych zespol_sprawdz = new BazaDanych();
            listBox1.Items.Clear();
            int ile_kolumn = 1;
            List<string> test = zespol_sprawdz.rekordy_wiecej_niz_1("SELECT DISTINCT adres_loc from zespol", ile_kolumn);
            List<string> druga = zespol_sprawdz.rekordy_wiecej_niz_1("SELECT DISTINCT id_zespol from zespol", ile_kolumn);
            int wynik = int.Parse(zespol_sprawdz.zwroc_rekord("Select max(id_zespol) from zespol"));
            
            for(int i=0;i<wynik;i++)
            {
                listBox1.Items.Add(druga[i]+"   "+test[i]);
            }
        }
        private void Zespol_load(object sender, EventArgs e)
        {

            BazaDanych zespol_sprawdz = new BazaDanych();
            int ile_kolum = 3;
            List<string> tets = zespol_sprawdz.rekordy_wiecej_niz_1("Select ID_prac ,Imie,Nazwisko from pracownik", ile_kolum);
            string wynki = "";
            int ik = 0;
            int kk = 0;
            foreach (string item in tets)
            {
                wynki += item;
                if (ik == 0)
                {
                    kk = 5 - item.Length;
                }
                if (ik == 1)
                {
                    kk = 14 - item.Length;
                }
                if (ik == 2)
                {
                    kk = 20 - item.Length;
                }
                if (ik == 3)
                {
                    kk = 5 - item.Length;
                }
                for (int j = 0; j < kk; j++)
                {
                    wynki += " ";
                }
                ik++;
                if (ik % ile_kolum == 0)
                {
                    comboBox1.Items.Add(wynki);
                    wynki = "";
                    ik = 0;
                }
            }

            odswierz();

        }

        public void Button1_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox3.Visible = true;
            comboBox1.Visible = true;
            string str1 = textBox1.Text;
            string str2 = textBox3.Text;
            BazaDanych dodaj_zesp = new BazaDanych();
            if (comboBox1.SelectedIndex == -1 || str1 == null || str2 == null)
            {
                MessageBox.Show("Nie wybrano danych");
            }
            else if(str2.Length>100)
            {
                MessageBox.Show("Za długi adres lokacji(Maksymalnie 100 znaków)");
            }
            else
            {
                string prac = comboBox1.SelectedItem.ToString();
                string[] rozlorzone = prac.Split(new char[] { ' ' });
                string polecenie = "INSERT INTO `zespol` (`id_prac`, `id_zespol`, `adres_loc`) VALUES('" + rozlorzone[0] + "', '" + textBox1.Text + "','" + textBox3.Text + "');";
                if (dodaj_zesp.wykonaj_polecenie(polecenie))
                {
                    MessageBox.Show("Pomyślnie dodano zespół");
                }
                else
                {
                    MessageBox.Show("Błąd dodawania zespołu");
                }
                comboBox1.SelectedIndex = -1;
                odswierz();
            }
            
        }

    
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            BazaDanych wyswietl_prac = new BazaDanych();
            if(listBox1.SelectedIndex==-1)
            {
                MessageBox.Show("Pierw musisz wybrać zespół");
            }
            else { 
            string id=listBox1.Items[listBox1.SelectedIndex].ToString().Split(new char[] { ' ' })[0];
            int ile_kolumn = 5;
            List<string> test = wyswietl_prac.rekordy_wiecej_niz_1("Select ID_prac,Imie,Nazwisko,Wiek,Stanowisko from pracownik where ID_prac in(Select id_prac from zespol where id_zespol="+id+");", ile_kolumn);
            string wynik = "";
            int i = 0;
            int k = 0;
                foreach (string item in test)
                {
                    wynik += item;
                    if (i == 0)
                    {
                        k = 5 - item.Length;
                    }
                    if (i == 1)
                    {
                        k = 14 - item.Length;
                    }
                    if (i == 2)
                    {
                        k = 20 - item.Length;
                    }
                    if (i == 3)
                    {
                        k = 5 - item.Length;
                    }
                    for (int j = 0; j < k; j++)
                    {
                        wynik += " ";
                    }
                    i++;
                    if (i % ile_kolumn == 0)
                    {
                        listBox2.Items.Add(wynik);
                        wynik = "";
                        i = 0;
                    }
                }
            }
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void labHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int indeks = listBox1.SelectedIndex;
            if (indeks == -1)
            {
                MessageBox.Show("Wybierz zespół do usunięcia");
            }
            else
            {
                string id = listBox1.Items[indeks].ToString().Split(new char[] { ' ' })[0];
                BazaDanych usun_zesp = new BazaDanych();
                string polecenie = "Delete from zespol where id_zespol=" + id + ";";
                if (usun_zesp.wykonaj_polecenie(polecenie))
                {
                    MessageBox.Show("Pomyslnie usunięto zespol o id " + id);
                }
                else
                {
                    MessageBox.Show("Usuwanie nie powiodło się");
                }
                odswierz();
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                string[] skladowe = listBox1.Items[listBox1.SelectedIndex].ToString().Split(new char[] { ' ' });
                textBox1.Text = skladowe[0];
                textBox3.Text = skladowe[skladowe.Length - 3] + " " +skladowe[skladowe.Length - 2] + " " + skladowe[skladowe.Length - 1];
                this.zespolu_id = skladowe[0];
                button3.PerformClick();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox3.Visible = true;
            comboBox1.Visible = true;
            string nowa_lokacja = textBox3.Text;
            if (nowa_lokacja.Length > 100)
            {
                MessageBox.Show("Nowa lokacja jest za długa(maks 100 znaków)");
            }
            else
            {
                string id = textBox1.Text;
                if (id == null || nowa_lokacja == null)
                {
                    MessageBox.Show("Przed zmianą lokacji uzupełnij pola");
                }
                else
                {
                    BazaDanych modyfikuj = new BazaDanych();
                    string polecenie = "Update zespol set adres_loc='" + nowa_lokacja + "' where id_zespol=" + id + ";";
                    if (modyfikuj.wykonaj_polecenie(polecenie))
                    {
                        MessageBox.Show("Pomyslnie zmieniono lokacje");
                    }
                    else
                    {
                        MessageBox.Show("Nie udało się zmienić zespolu");
                    }
                    odswierz();
                }
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string id_zespol = textBox1.Text;
            if (comboBox1.SelectedIndex == -1 || id_zespol == null)
            {
                MessageBox.Show("nie wybrano pracownika lub/i id zespołu");
            }
            else
            {
                string prac = comboBox1.SelectedItem.ToString();
                string[] rozlorzone = prac.Split(new char[] { ' ' });
                string id_prac = rozlorzone[0];
                BazaDanych dodaj_prac = new BazaDanych();
                string lokacja = dodaj_prac.zwroc_rekord("select adres_loc from zespol where id_zespol=" + id + ";");
                string komenda = "INSERT INTO `zespol` (`id_prac`, `id_zespol`, `adres_loc`) VALUES ('" + id_prac + "','" + id_zespol + "','" + lokacja + "');";
                if (dodaj_prac.wykonaj_polecenie(komenda))
                {
                    MessageBox.Show("Pomyslnie dodano pracownika do zespołu");

                }
                else
                {
                    MessageBox.Show("Coś nie pykło");
                }
                button3.PerformClick();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Wybierz zespół");
            }
            else
            {
                if (listBox2.SelectedIndex == -1)
                {
                    MessageBox.Show("wybierz pracownika z listy");
                }
                else
                {
                    string wybrany = listBox2.SelectedItem.ToString();
                    string id = wybrany.Split(new char[] { ' ' })[0];
                    string id_zespol = textBox1.Text;
                    string zapytanie = "DELETE FROM zespol WHERE id_prac = '" + Int32.Parse(id) + "'AND id_zespol = '" + Int32.Parse(id_zespol) + "';";
                    BazaDanych usun_prac = new BazaDanych();
                    if (usun_prac.wykonaj_polecenie(zapytanie))
                    {
                        MessageBox.Show("Pomyslnie usunieto pracownika z zespolu");
                    }
                    else
                    {
                        MessageBox.Show("Nie pykło");
                    }
                    button3.PerformClick();
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if(int.Parse(this.zespolu_id)!=-1)
            {
                Zadania zadanko = new Zadania(this.zespolu_id);
                zadanko.ShowDialog();
            }
            else
            {
                MessageBox.Show("Musisz zaznaczyc zespol!");
            }
        }
    }
}
