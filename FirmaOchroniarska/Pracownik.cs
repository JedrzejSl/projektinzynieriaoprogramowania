﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirmaOchroniarska
{
    public partial class Pracownik : Form
    {
        //int id = 0;
        public Pracownik()
        {
            InitializeComponent();
            this.odswiezenie();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dodaj_prac pracc = new Dodaj_prac();
            pracc.ShowDialog();
            this.odswiezenie();
        }


        private void labHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)//usuwanie
        {
            listBox1.Items.Clear();
            BazaDanych wyswietl_prac = new BazaDanych();
            int ile_kolumn = 5;
            List<string> test = wyswietl_prac.rekordy_wiecej_niz_1("Select ID_prac,Imie,Nazwisko,Wiek,Stanowisko from pracownik", ile_kolumn);
            string wynik = "";
            int i = 0;
            int k = 0;
            foreach (string item in test)
            {
                wynik += item;
                if (i == 0)
                {
                    k = 5 - item.Length;
                }
                if (i == 1)
                {
                    k = 14 - item.Length;
                }
                if (i == 2)
                {
                    k = 20 - item.Length;
                }
                if (i == 3)
                {
                    k = 5 - item.Length;
                }
                for (int j = 0; j < k; j++)
                {
                    wynik += " ";
                }
                i++;
                if (i % ile_kolumn == 0)
                {
                    listBox1.Items.Add(wynik);
                    wynik = "";
                    i = 0;
                }
            }
        }

        private void btn_usun_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Musisz wybrac pracownika do usunięcia");
            }
            else
            {
                int numer = listBox1.SelectedIndex;
                string wiersz = listBox1.Items[numer].ToString();
                List<string> lista_el = wiersz.Split(' ').ToList();
                BazaDanych usuwanko = new BazaDanych();
                DialogResult odp = MessageBox.Show("Czy na pewno chcesz usunac wybranego pracownika?", "Usuwanie", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (odp == DialogResult.Yes)
                {
                    if (usuwanko.wykonaj_polecenie("DELETE FROM pracownik WHERE ID_prac='" + lista_el[0] + "';" + "DELETE FROM logowanie WHERE ID_prac='" + lista_el[0] + "';"))
                    {
                        MessageBox.Show("Usunięto wybranego pracownika");
                        listBox1.Items.RemoveAt(numer);
                    }
                    else
                    {
                        MessageBox.Show("Coś poszło nie tak");
                    }
                }
            }
        }

        private void odswiezenie()
        {
            listBox1.Items.Clear();
            BazaDanych wyswietl_prac = new BazaDanych();
            int ile_kolumn = 5;
            List<string> test = wyswietl_prac.rekordy_wiecej_niz_1("Select ID_prac,Imie,Nazwisko,Wiek,Stanowisko from pracownik", ile_kolumn);
            string wynik = "";
            int i = 0;
            int k = 0;
            foreach (string item in test)
            {
                wynik += item;
                if (i == 0)
                {
                    k = 5 - item.Length;
                }
                if (i == 1)
                {
                    k = 14 - item.Length;
                }
                if (i == 2)
                {
                    k = 20 - item.Length;
                }
                if (i == 3)
                {
                    k = 5 - item.Length;
                }
                for (int j = 0; j < k; j++)
                {
                    wynik += " ";
                }
                i++;
                if (i % ile_kolumn == 0)
                {
                    listBox1.Items.Add(wynik);
                    wynik = "";
                    i = 0;
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)//edycja
        {
            if (listBox1.SelectedIndex == -1)
            {
                MessageBox.Show("Musisz wybrac pracownika do edycji");
            }
            else
            {
                combo_stan.Visible = true;
                txtImie.Visible = true;
                txtNazwisko.Visible = true;
                txtWiek.Visible = true;
                label7.Visible = true;
                label8.Visible = true;
                label9.Visible = true; 
                label10.Visible = true;
                txt_dod.Visible = true;
                btn_zapisz_edycja.Visible = true;
                int numer = listBox1.SelectedIndex;
                string wiersz = listBox1.Items[numer].ToString();
                List<string> lista_el = wiersz.Split(' ').ToList();
                List<string> ost_list = new List<string>();
                for (int i= 0; i < lista_el.Count;i++)
                {
                    if(lista_el[i]!="")
                    {
                        if(lista_el[i]=="szef")
                        {
                            ost_list.Add(lista_el[i] + " " + lista_el[i + 1]);
                            break;
                        }
                        ost_list.Add(lista_el[i]);
                    }
                }
                BazaDanych edycja = new BazaDanych();
                txtImie.Text = ost_list[1];
                txtNazwisko.Text = ost_list[2];
                txtWiek.Text = ost_list[3];
                combo_stan.Text = ost_list[4];
                this.combo_stan_SelectedIndexChanged();
                if (combo_stan.Text == "ochroniarz" || combo_stan.Text == "szef ochrony" || combo_stan.Text == "szef zmiany")
                {   
                    if (combo_stan.SelectedItem.ToString() == "ochroniarz")
                    {
                        txt_dod.Text = edycja.zwroc_rekord("SELECT nr_licencji from ochroniarze where ID_prac='" + ost_list[0]+"';"); 
                    }
                    if (combo_stan.SelectedItem.ToString() == "szef ochrony")
                    {
                        txt_dod.Text = edycja.zwroc_rekord("SELECT Telefon from szef_ochrony where ID_prac='" + ost_list[0] + "';");
                    }
                    if (combo_stan.SelectedItem.ToString() == "szef zmiany")
                    {
                        txt_dod.Text = edycja.zwroc_rekord("SELECT numer_zmiany from szef_zmiany where ID_prac='" + ost_list[0] + "';");
                    }        
                }
                else
                {
                    MessageBox.Show("Wybierz rodzaj pracownika przed edycją");
                }
            }
        }

        private void btn_zapisz_edycja_Click(object sender, EventArgs e)
        {
            bool dobrze = true;
            int numer = listBox1.SelectedIndex;
            string wiersz = listBox1.Items[numer].ToString();
            List<string> lista_el = wiersz.Split(' ').ToList();

            if(txtImie.Text.Length>20)
            {
                dobrze = false;
                MessageBox.Show("Imię nie może być dłuższe niż 20 znaków");
            }
            if (txtImie.Text.Length == 0)
            {
                dobrze = false;
                MessageBox.Show("Imię nie może być puste");
            }
            if (txtNazwisko.Text.Length > 50)
            {
                dobrze = false;
                MessageBox.Show("Nazwisko nie może być dłuższe niż 50 znaków");
            }
            if (txtNazwisko.Text.Length == 0)
            {
                dobrze = false;
                MessageBox.Show("Nazwisko nie może być puste");
            }
            if (int.TryParse(txtWiek.Text, out int parsted))
            {
                if (txtWiek.Text.Length > 3)
                {
                    dobrze = false;
                    MessageBox.Show("Wiek nie może być dłuższy niż 3 znaki");
                }
                if (txtWiek.Text.Length == 0)
                {
                    dobrze = false;
                    MessageBox.Show("Nazwisko nie może być puste");
                }
                if (int.Parse(txtWiek.Text) < 18)
                {
                    dobrze = false;
                    MessageBox.Show("Nie można zatrudniać dzieci");
                }
            }
            else
            {
                dobrze = false;
                MessageBox.Show("Wiek jest liczba a nie znakami!");
            }
            if (txt_dod.Text.Length > 9)
            {
                dobrze = false;
                MessageBox.Show("Informacja dodatkowa(nr legitymacji, numer zmiany, numer telefonu\nnie moze byc dluzsza niz 9 znakow");
            }
            if (txt_dod.Text.Length == 0)
            {
                dobrze = false;
                MessageBox.Show("Informacja dodatkowa(nr legitymacji, numer zmiany, numer telefonu\nnie moze byc pusta");
            }

            if (dobrze)
            {
                if (combo_stan.Text == "ochroniarz" || combo_stan.Text == "szef ochrony" || combo_stan.Text == "szef zmiany")
                {
                    BazaDanych edycja2 = new BazaDanych();
                    if (combo_stan.SelectedItem.ToString() == "ochroniarz")
                    {
                        string stare_stanowisko = edycja2.zwroc_rekord("SELECT Stanowisko from pracownik where ID_prac='" + lista_el[0] + "';");
                        if (stare_stanowisko == "ochroniarz")//bez zmian
                        {
                            edycja2.wykonaj_polecenie("Update ochroniarze set nr_licencji ='" + txt_dod.Text + "' where ID_prac='" + lista_el[0] + "';");
                        }
                        if (stare_stanowisko == "szef zmiany")//szefzmiany->ochroniarz
                        {
                            edycja2.wykonaj_polecenie("Delete from szef_zmiany where ID_prac='" + lista_el[0] + "';");
                            edycja2.wykonaj_polecenie("Insert into ochroniarze values ('" + lista_el[0] + "','" + txt_dod.Text + "');");
                        }
                        if (stare_stanowisko == "szef ochrony")//szefochrony->ochroniarz
                        {
                            edycja2.wykonaj_polecenie("Delete from szef_ochrony where ID_prac='" + lista_el[0] + "';");
                            edycja2.wykonaj_polecenie("Insert into ochroniarze values ('" + lista_el[0] + "','" + txt_dod.Text + "');");
                        }
                    }
                    if (combo_stan.SelectedItem.ToString() == "szef ochrony")
                    {
                        string stare_stanowisko = edycja2.zwroc_rekord("SELECT Stanowisko from pracownik where ID_prac='" + lista_el[0] + "';");
                        if (stare_stanowisko == "szef ochrony")//bez zmian
                        {
                            edycja2.wykonaj_polecenie("Update szef_ochrony set Telefon ='" + txt_dod.Text + "' where ID_prac='" + lista_el[0] + "';");
                        }
                        if (stare_stanowisko == "szef zmiany")//szefzmiany->szef_ochrony
                        {
                            edycja2.wykonaj_polecenie("Delete from szef_zmiany where ID_prac='" + lista_el[0] + "';");
                            edycja2.wykonaj_polecenie("Insert into szef_ochrony values ('" + lista_el[0] + "','" + txt_dod.Text + "');");
                        }
                        if (stare_stanowisko == "ochroniarz")//ochroniarz->szef_ochrony
                        {
                            edycja2.wykonaj_polecenie("Delete from ochroniarze where ID_prac='" + lista_el[0] + "';");
                            edycja2.wykonaj_polecenie("Insert into szef_ochrony values ('" + lista_el[0] + "','" + txt_dod.Text + "');");
                        }
                    }
                    if (combo_stan.SelectedItem.ToString() == "szef zmiany")
                    {
                        string stare_stanowisko = edycja2.zwroc_rekord("SELECT Stanowisko from pracownik where ID_prac='" + lista_el[0] + "';");
                        if (stare_stanowisko == "szef zmiany")//bez zmian
                        {
                            edycja2.wykonaj_polecenie("Update szef_zmiany set numer_zmiany ='" + txt_dod.Text + "' where ID_prac='" + lista_el[0] + "';");
                        }
                        if (stare_stanowisko == "ochroniarz")//ochroniarz->szef zmiany
                        {
                            edycja2.wykonaj_polecenie("Delete from ochroniarze where ID_prac='" + lista_el[0] + "';");
                            edycja2.wykonaj_polecenie("Insert into szef_zmiany values ('" + lista_el[0] + "','" + txt_dod.Text + "');");
                        }
                        if (stare_stanowisko == "szef ochrony")//szefochrony->szef_zmiany
                        {
                            edycja2.wykonaj_polecenie("Delete from szef_ochrony where ID_prac='" + lista_el[0] + "';");
                            edycja2.wykonaj_polecenie("Insert into szef_zmiany values ('" + lista_el[0] + "','" + txt_dod.Text + "');");
                        }
                    }
                    edycja2.wykonaj_polecenie("Update pracownik set Imie='" + txtImie.Text + "', Nazwisko='" + txtNazwisko.Text + "', Wiek='" + txtWiek.Text + "', Stanowisko='" + combo_stan.Text + "' where ID_prac='" + lista_el[0] + "';");
                    this.odswiezenie();
                    MessageBox.Show("Edytowano pracownika");
                }
                else
                {
                    MessageBox.Show("Niepoprawny rodzaj pracownika");
                }
            }
        }

        private void combo_stan_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (combo_stan.SelectedItem.ToString() == "ochroniarz")
            {
                label12.Visible = true;
                label11.Visible = false;
                label13.Visible = false;
            }
            if (combo_stan.SelectedItem.ToString() == "szef ochrony")
            {
                label13.Visible = true;
                label12.Visible = false;
                label11.Visible = false;
            }
            if (combo_stan.SelectedItem.ToString() == "szef zmiany")
            {
                label11.Visible = true;
                label12.Visible = false;
                label13.Visible = false;
            }
            txt_dod.Text = "";
        }
        private void combo_stan_SelectedIndexChanged()
        {
            if (combo_stan.SelectedItem.ToString() == "ochroniarz")
            {
                label12.Visible = true;
                label11.Visible = false;
                label13.Visible = false;
            }
            if (combo_stan.SelectedItem.ToString() == "szef ochrony")
            {
                label13.Visible = true;
                label12.Visible = false;
                label11.Visible = false;
            }
            if (combo_stan.SelectedItem.ToString() == "szef zmiany")
            {
                label11.Visible = true;
                label12.Visible = false;
                label13.Visible = false;
            }
            txt_dod.Text = "";
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            combo_stan.Visible = false;
            txtImie.Visible = false;
            txtNazwisko.Visible = false;
            txtWiek.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label10.Visible = false;
            txt_dod.Visible = false;
            btn_zapisz_edycja.Visible = false;
            label11.Visible = false;
            label12.Visible = false;
            label13.Visible = false;
        }
    }
}

