﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirmaOchroniarska
{
    public partial class Notatka : Form
    {
        public string id;
        public Notatka(string id_z_main)
        {
            InitializeComponent();
            this.id = id_z_main;
            Load += new EventHandler(Notatka_Load);
        }
        private void Notatka_Load(object sender, System.EventArgs e)
        {
            BazaDanych start = new BazaDanych();
            string zap = "select Notatka from pracownik where ID_prac=" + id + ";";
            textBox1.Text = start.zwroc_rekord(zap);
            
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool dobrze = true;
            if(textBox1.Text.Length>1000)
            {
                dobrze = false;
                MessageBox.Show("Notatka nie moze byc dluższa niż 1000 znaków");
            }
            if (dobrze)
            {
                BazaDanych poloncz = new BazaDanych();
                string zTB = textBox1.Text;
                string zapytanie = "Update pracownik set Notatka='" + zTB + "' where ID_prac=" + id + ";";
                if (poloncz.wykonaj_polecenie(zapytanie))
                {
                    MessageBox.Show("Pomyślnie zapisano notatke");
                }
                else
                {
                    MessageBox.Show("Nie pykło");
                }
            }
        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void labHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
