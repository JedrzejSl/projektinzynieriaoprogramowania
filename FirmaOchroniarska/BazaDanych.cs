﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Diagnostics;

namespace FirmaOchroniarska
{
    class BazaDanych
    {
        MySqlConnection loc;
        public BazaDanych()
        {
            this.loc = new MySqlConnection("Server = remotemysql.com; port = 3306; database = JMPlhG0o15; UID = JMPlhG0o15; password = KqPn9Ylaet; CharSet = utf8");
        }

        public bool true_false_zapytanie(string zapytanko)
        {
            MySqlCommand zapytanie = new MySqlCommand(zapytanko, this.loc);
            this.loc.Open();
            MySqlDataReader czytacz;
            czytacz = zapytanie.ExecuteReader();

            if (czytacz.Read())
            {
                czytacz.Close();
                this.loc.Close();
                return true;
            }
            czytacz.Close();
            this.loc.Close();
            return false;
        }

        public string zwroc_rekord(string zapytanko)
        {
            MySqlCommand zapytanie = new MySqlCommand(zapytanko, this.loc);
            this.loc.Open();
            MySqlDataReader czytacz;
            try
            {
                czytacz = zapytanie.ExecuteReader();
                if (czytacz.Read())
                {
                    string zwrot = czytacz.GetString(0);
                    czytacz.Close();
                    this.loc.Close();
                    return zwrot;
                }
            }
            catch
            {
                this.loc.Close();
                return "";
            }
            this.loc.Close();
            return "";
        }

        public bool wykonaj_polecenie(string polecenie)
        {
            MySqlCommand zapytanie = new MySqlCommand(polecenie, this.loc);
            this.loc.Open();
            bool udalo_sie;
            zapytanie.CommandText = polecenie;
            if (zapytanie.ExecuteNonQuery() > 0)
            {
                udalo_sie = true;
            }
            else
            {
                udalo_sie = false;
            }
            this.loc.Close();
            return udalo_sie;
        }
        public List<List<string>> zwroc_pracownik(string zapytanko, int numer)
        {
            List<string> id=new List<string>();
            List<string> imie=new List<string>();
            List<string> nazwisko = new List<string>();
            List<string> wiek = new List<string>();
            List<string> notatka = new List<string>();
            List<string> stanowisko = new List<string>();
            DataTable tmp = new DataTable();
            List<List<string>> wynik = new List<List<string>>();
            MySqlCommand zapytanie = new MySqlCommand(zapytanko, this.loc);
            this.loc.Open();
                var adapter = new MySqlDataAdapter(zapytanie);
                adapter.Fill(tmp);

                    foreach(DataRow rzad in tmp.Rows)
                    {
                        id.Add(rzad["ID_prac"].ToString());
                        imie.Add(rzad["Imie"].ToString());
                        nazwisko.Add(rzad["Nazwisko"].ToString());
                        wiek.Add(rzad["Wiek"].ToString());
                        notatka.Add(rzad["Notatka"].ToString());
                        stanowisko.Add(rzad["Stanowisko"].ToString());
                       
                    }
            if (numer < id.Count-1)
            {
                Debug.WriteLine(id[numer] + " " + imie[numer] + " " + nazwisko[numer] + " " + wiek[numer] + " " + notatka[numer] + " " + stanowisko[numer]);
            }
            else
            {
                Debug.WriteLine("Koniec danych");
            }
            this.loc.Close();
            wynik.Add(id);
            wynik.Add(imie);
            wynik.Add(nazwisko);
            wynik.Add(wiek);
            wynik.Add(notatka);
            wynik.Add(stanowisko);
            return wynik;
        }
        public List<List<string>> zwroc_zespol(string zapytanko, int numer)
        {
            List<string> id = new List<string>();
            List<string> id_zesp = new List<string>();
            List<string> adr_lok = new List<string>();
            List<string> imie = new List<string>();
            List<string> nazwisko = new List<string>();
            List<string> stanowisko = new List<string>();
            List<string> id_zarzadz = new List<string>();//id do sprawdzania czy parcownik jest w zespole
            DataTable tmp = new DataTable();
            List<List<string>> wynik = new List<List<string>>();
            MySqlCommand zapytanie = new MySqlCommand(zapytanko, this.loc);
            this.loc.Open();
            int numer2 = 0;
            int max = Convert.ToInt32(zapytanie.ExecuteScalar());
            var adapter = new MySqlDataAdapter(zapytanie);
            adapter.Fill(tmp);

            foreach (DataRow rzad in tmp.Rows)
            {
                id.Add(rzad["id_prac"].ToString());
                id_zesp.Add(rzad["id_zespol"].ToString());
                adr_lok.Add(rzad["adres_loc"].ToString());

            }
            string str2 = "Select ID_prac, Imie, Nazwisko, Stanowisko from pracownik WHERE ID_prac IN (Select id_prac from zespol);";
            MySqlCommand zapytanie2 = new MySqlCommand(str2, this.loc);
            var adapter2 = new MySqlDataAdapter(zapytanie2);
            DataTable tmp2 = new DataTable();
            adapter2.Fill(tmp2);
            foreach (DataRow rzad in tmp2.Rows)
            {
                imie.Add(rzad["Imie"].ToString());
                nazwisko.Add(rzad["Nazwisko"].ToString());
                stanowisko.Add(rzad["Stanowisko"].ToString());
                id_zarzadz.Add(rzad["ID_prac"].ToString());
            }
            
            this.loc.Close();
           
            if (id_zarzadz[numer2] == id[numer]) {
                while (numer < id.Count&&numer2<imie.Count)
                {
                    Debug.WriteLine(id_zesp[numer] + " " + adr_lok[numer]+" " +imie[numer2]+" "+nazwisko[numer2]+" "+stanowisko[numer2]);
                    numer2++;
                }
            }
            wynik.Add(id_zesp);
            wynik.Add(adr_lok);
            wynik.Add(imie);
            wynik.Add(nazwisko);
            wynik.Add(stanowisko);
            return wynik;
        }

        public List<string> rekordy_wiecej_niz_1(string zapytanko, int ile_kolumn)
        {
            MySqlCommand zapytanie = new MySqlCommand(zapytanko, this.loc);
            this.loc.Open();
            MySqlDataReader czytacz;
            List<string> lista = new List<string>();
            try
            {
                czytacz = zapytanie.ExecuteReader();
                while (czytacz.Read())
                {
                    for (int j = 0; j < ile_kolumn; j++)
                    {
                        lista.Add(czytacz.GetString(j));
                    }
                }
                czytacz.Close();
                this.loc.Close();
                return lista;
            }
            catch
            {
                this.loc.Close();
                return lista;
            }
        }
    }
}
