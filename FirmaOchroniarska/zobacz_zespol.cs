﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirmaOchroniarska
{
    public partial class zobacz_zespol : Form
    {
        string id;
        public zobacz_zespol(string id_z_home)
        {
            InitializeComponent();
            this.id = id_z_home;
        }

        private void zobacz_zespol_Load(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            BazaDanych wyswietl_prac = new BazaDanych();
            string id_zespol = wyswietl_prac.zwroc_rekord("select id_zespol from zespol where id_prac='"+this.id+"';");
            int ile_kolumn = 2;
            List<string> test = wyswietl_prac.rekordy_wiecej_niz_1("Select Imie,Nazwisko from pracownik where ID_prac in(Select id_prac from zespol where id_zespol=" + id_zespol + ");", ile_kolumn);
            string wynik = "";
            int i = 0;
            int k = 0;
            foreach (string item in test)
            {
                wynik += item;
                if (i == 0)
                {
                    k = 11 - item.Length;
                }
                for (int j = 0; j < k; j++)
                {
                    wynik += " ";
                }
                i++;
                if (i % ile_kolumn == 0)
                {
                    listBox1.Items.Add(wynik);
                    wynik = "";
                    i = 0;
                }
            }
            label3.Text=wyswietl_prac.zwroc_rekord("SELECT DISTINCT adres_loc from zespol where id_prac='" + this.id+"';");
            textBox1.Text = wyswietl_prac.zwroc_rekord("SELECT DISTINCT tresc_zadania from zespol where id_prac='" + this.id + "';");
            label6.Text= wyswietl_prac.zwroc_rekord("SELECT DISTINCT data_zadania from zespol where id_prac='" + this.id + "';");
        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void labHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   
    }
}
