﻿namespace FirmaOchroniarska
{
    partial class Grafik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labWylog = new System.Windows.Forms.Label();
            this.labPom = new System.Windows.Forms.Label();
            this.lab_opcje = new System.Windows.Forms.Label();
            this.labHome = new System.Windows.Forms.Label();
            this.labInfoGrafik = new System.Windows.Forms.Label();
            this.btnPrzyszly = new System.Windows.Forms.Button();
            this.labMiesiac = new System.Windows.Forms.Label();
            this.picGrafik = new System.Windows.Forms.PictureBox();
            this.btnDodajGrafik = new System.Windows.Forms.Button();
            this.btnEdytujGrafik = new System.Windows.Forms.Button();
            this.btnUsunGrafik = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picGrafik)).BeginInit();
            this.SuspendLayout();
            // 
            // labWylog
            // 
            this.labWylog.AutoSize = true;
            this.labWylog.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labWylog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labWylog.Location = new System.Drawing.Point(645, 19);
            this.labWylog.Name = "labWylog";
            this.labWylog.Size = new System.Drawing.Size(64, 16);
            this.labWylog.TabIndex = 40;
            this.labWylog.Text = "Wyloguj";
            this.labWylog.Click += new System.EventHandler(this.labWylog_Click);
            // 
            // labPom
            // 
            this.labPom.AutoSize = true;
            this.labPom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labPom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labPom.Location = new System.Drawing.Point(577, 19);
            this.labPom.Name = "labPom";
            this.labPom.Size = new System.Drawing.Size(56, 16);
            this.labPom.TabIndex = 39;
            this.labPom.Text = "Pomoc";
            // 
            // lab_opcje
            // 
            this.lab_opcje.AutoSize = true;
            this.lab_opcje.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lab_opcje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lab_opcje.Location = new System.Drawing.Point(477, 13);
            this.lab_opcje.Name = "lab_opcje";
            this.lab_opcje.Size = new System.Drawing.Size(94, 32);
            this.lab_opcje.TabIndex = 38;
            this.lab_opcje.Text = "Opcje \r\nUżytkownika";
            // 
            // labHome
            // 
            this.labHome.AutoSize = true;
            this.labHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labHome.Location = new System.Drawing.Point(409, 13);
            this.labHome.Name = "labHome";
            this.labHome.Size = new System.Drawing.Size(62, 32);
            this.labHome.TabIndex = 37;
            this.labHome.Text = "Strona \r\nGłówna";
            this.labHome.Click += new System.EventHandler(this.labHome_Click);
            // 
            // labInfoGrafik
            // 
            this.labInfoGrafik.AutoSize = true;
            this.labInfoGrafik.Font = new System.Drawing.Font("Bookman Old Style", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labInfoGrafik.Location = new System.Drawing.Point(188, 77);
            this.labInfoGrafik.Name = "labInfoGrafik";
            this.labInfoGrafik.Size = new System.Drawing.Size(283, 27);
            this.labInfoGrafik.TabIndex = 41;
            this.labInfoGrafik.Text = "Twój grafik na miesiąc:\r\n";
            // 
            // btnPrzyszly
            // 
            this.btnPrzyszly.BackColor = System.Drawing.Color.GhostWhite;
            this.btnPrzyszly.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrzyszly.Enabled = false;
            this.btnPrzyszly.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnPrzyszly.FlatAppearance.BorderSize = 2;
            this.btnPrzyszly.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrzyszly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPrzyszly.Location = new System.Drawing.Point(220, 279);
            this.btnPrzyszly.Name = "btnPrzyszly";
            this.btnPrzyszly.Size = new System.Drawing.Size(251, 34);
            this.btnPrzyszly.TabIndex = 43;
            this.btnPrzyszly.Text = "Zobacz grafik na kolejny miesiąc";
            this.btnPrzyszly.UseVisualStyleBackColor = false;
            this.btnPrzyszly.Click += new System.EventHandler(this.btnPrzyszly_Click);
            // 
            // labMiesiac
            // 
            this.labMiesiac.AutoSize = true;
            this.labMiesiac.Font = new System.Drawing.Font("Bookman Old Style", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labMiesiac.Location = new System.Drawing.Point(262, 104);
            this.labMiesiac.Name = "labMiesiac";
            this.labMiesiac.Size = new System.Drawing.Size(132, 27);
            this.labMiesiac.TabIndex = 44;
            this.labMiesiac.Text = "Luty 2020";
            // 
            // picGrafik
            // 
            this.picGrafik.Image = global::FirmaOchroniarska.Properties.Resources.grafik;
            this.picGrafik.Location = new System.Drawing.Point(106, 158);
            this.picGrafik.Name = "picGrafik";
            this.picGrafik.Size = new System.Drawing.Size(500, 100);
            this.picGrafik.TabIndex = 42;
            this.picGrafik.TabStop = false;
            // 
            // btnDodajGrafik
            // 
            this.btnDodajGrafik.BackColor = System.Drawing.Color.GhostWhite;
            this.btnDodajGrafik.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnDodajGrafik.FlatAppearance.BorderSize = 2;
            this.btnDodajGrafik.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDodajGrafik.Location = new System.Drawing.Point(24, 18);
            this.btnDodajGrafik.Name = "btnDodajGrafik";
            this.btnDodajGrafik.Size = new System.Drawing.Size(83, 27);
            this.btnDodajGrafik.TabIndex = 45;
            this.btnDodajGrafik.Text = "Dodaj Grafik";
            this.btnDodajGrafik.UseVisualStyleBackColor = false;
            this.btnDodajGrafik.Visible = false;
            // 
            // btnEdytujGrafik
            // 
            this.btnEdytujGrafik.BackColor = System.Drawing.Color.GhostWhite;
            this.btnEdytujGrafik.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnEdytujGrafik.FlatAppearance.BorderSize = 2;
            this.btnEdytujGrafik.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdytujGrafik.Location = new System.Drawing.Point(113, 18);
            this.btnEdytujGrafik.Name = "btnEdytujGrafik";
            this.btnEdytujGrafik.Size = new System.Drawing.Size(83, 27);
            this.btnEdytujGrafik.TabIndex = 46;
            this.btnEdytujGrafik.Text = "Edytuj Grafik";
            this.btnEdytujGrafik.UseVisualStyleBackColor = false;
            this.btnEdytujGrafik.Visible = false;
            // 
            // btnUsunGrafik
            // 
            this.btnUsunGrafik.BackColor = System.Drawing.Color.GhostWhite;
            this.btnUsunGrafik.FlatAppearance.BorderColor = System.Drawing.Color.LightGray;
            this.btnUsunGrafik.FlatAppearance.BorderSize = 2;
            this.btnUsunGrafik.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUsunGrafik.Location = new System.Drawing.Point(202, 18);
            this.btnUsunGrafik.Name = "btnUsunGrafik";
            this.btnUsunGrafik.Size = new System.Drawing.Size(81, 27);
            this.btnUsunGrafik.TabIndex = 47;
            this.btnUsunGrafik.Text = "Usuń Grafik";
            this.btnUsunGrafik.UseVisualStyleBackColor = false;
            this.btnUsunGrafik.Visible = false;
            // 
            // Grafik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(721, 450);
            this.Controls.Add(this.btnUsunGrafik);
            this.Controls.Add(this.btnEdytujGrafik);
            this.Controls.Add(this.btnDodajGrafik);
            this.Controls.Add(this.labMiesiac);
            this.Controls.Add(this.btnPrzyszly);
            this.Controls.Add(this.picGrafik);
            this.Controls.Add(this.labInfoGrafik);
            this.Controls.Add(this.labWylog);
            this.Controls.Add(this.labPom);
            this.Controls.Add(this.lab_opcje);
            this.Controls.Add(this.labHome);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Name = "Grafik";
            this.Text = "Grafik";
            this.Load += new System.EventHandler(this.Grafik_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picGrafik)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labWylog;
        private System.Windows.Forms.Label labPom;
        private System.Windows.Forms.Label lab_opcje;
        private System.Windows.Forms.Label labHome;
        private System.Windows.Forms.Label labInfoGrafik;
        private System.Windows.Forms.PictureBox picGrafik;
        private System.Windows.Forms.Button btnPrzyszly;
        private System.Windows.Forms.Label labMiesiac;
        private System.Windows.Forms.Button btnDodajGrafik;
        private System.Windows.Forms.Button btnEdytujGrafik;
        private System.Windows.Forms.Button btnUsunGrafik;
    }
}