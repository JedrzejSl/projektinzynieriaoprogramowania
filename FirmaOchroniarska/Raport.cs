﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using X14 = DocumentFormat.OpenXml.Office2010.Excel;
using X15 = DocumentFormat.OpenXml.Office2013.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirmaOchroniarska
{
    public partial class Raport : Form
    {
        string id;
        public Raport(string id_z_home)
        {
            InitializeComponent();
            this.id = id_z_home;
            BazaDanych stabowisko = new BazaDanych();
            string stanowisko = stabowisko.zwroc_rekord("SELECT Stanowisko from pracownik where ID_prac='" + this.id + "'");
            if(stanowisko!="szef ochrony")
            {
                button5.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dodaj_raport nowyRaport = new dodaj_raport(this.id);
            nowyRaport.ShowDialog();
        }


        private void labHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private static WorksheetPart InsertWorksheet(WorkbookPart workbookPart)
        {
            WorksheetPart newWorksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            newWorksheetPart.Worksheet = new Worksheet(new SheetData());
            newWorksheetPart.Worksheet.Save();

            Sheets sheets = workbookPart.Workbook.GetFirstChild<Sheets>();
            string relationshipId = workbookPart.GetIdOfPart(newWorksheetPart);

            uint sheetId = 1;
            if (sheets.Elements<Sheet>().Count() > 0)
            {
                sheetId = sheets.Elements<Sheet>().Select(s => s.SheetId.Value).Max() + 1;
            }

            string sheetName = "Arkusz" + sheetId;

            Sheet sheet = new Sheet() { Id = relationshipId, SheetId = sheetId, Name = sheetName };
            sheets.Append(sheet);
            workbookPart.Workbook.Save();

            return newWorksheetPart;
        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            string sciezka = @"C:\Raport.xls";
            StworzRaport(sciezka);
        }

        public static void StworzRaport(string sciezka)
        {
            SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Create(sciezka, SpreadsheetDocumentType.Workbook);

            WorkbookPart workbookpart = spreadsheetDocument.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();

            WorksheetPart worksheetPart = workbookpart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new SheetData());

            Sheets sheets = spreadsheetDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

            Sheet sheet = new Sheet() { Id = spreadsheetDocument.WorkbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = "raport" };
            sheets.Append(sheet);

            SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

            Row row;
            row = new Row() { RowIndex = 1 };
            sheetData.Append(row);

            Cell refCell = null;
            foreach (Cell cell in row.Elements<Cell>())
            {
                if (string.Compare(cell.CellReference.Value, "A1", true) > 0)
                {
                    refCell = cell;
                    break;
                }
            }
            Cell refCell2 = null;
            foreach (Cell cell in row.Elements<Cell>())
            {
                if (string.Compare(cell.CellReference.Value, "A1", true) > 0)
                {
                    refCell2 = cell;
                    break;
                }
            }
            BazaDanych baza = new BazaDanych();
            List<string> raporty = baza.rekordy_wiecej_niz_1("Select * from raport", 4);
            Cell newCell = new Cell() { CellReference = "A1" };
            row.InsertBefore(newCell, refCell);
            string pierwszyRaport = "Id pracownika: " + raporty[0] + "       data: " + raporty[1] + "      treść: " + raporty[2] + "       lokacja: " + raporty[3];
            string drugiRaport = "Id pracownika: " + raporty[4] + "       data: " + raporty[5] + "      treść: " + raporty[6] + "       lokacja: " + raporty[7];
            string trzeciRaport = "Id pracownika: " + raporty[8] + "       data: " + raporty[9] + "      treść: " + raporty[10] + "       lokacja: " + raporty[11];
            newCell.CellValue = new CellValue(pierwszyRaport);
            newCell.DataType = new EnumValue<CellValues>(CellValues.String);
            Cell cell2 = InsertCellInWorksheet("A", 2, worksheetPart);
            cell2.CellValue = new CellValue(drugiRaport);
            cell2.DataType = new EnumValue<CellValues>(CellValues.String);
            Cell cell3 = InsertCellInWorksheet("A", 3, worksheetPart);
            cell3.CellValue = new CellValue(trzeciRaport);
            cell3.DataType = new EnumValue<CellValues>(CellValues.String);
            spreadsheetDocument.Close();
        }


        private static Cell InsertCellInWorksheet(string columnName, uint rowIndex, WorksheetPart worksheetPart)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();
            string cellReference = columnName + rowIndex;

            Row row;
            if (sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).Count() != 0)
            {
                row = sheetData.Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
            }
            else
            {
                row = new Row() { RowIndex = rowIndex };
                sheetData.Append(row);
            }

            if (row.Elements<Cell>().Where(c => c.CellReference.Value == columnName + rowIndex).Count() > 0)
            {
                return row.Elements<Cell>().Where(c => c.CellReference.Value == cellReference).First();
            }
            else
            {
                Cell refCell = null;
                foreach (Cell cell in row.Elements<Cell>())
                {
                    if (string.Compare(cell.CellReference.Value, cellReference, true) > 0)
                    {
                        refCell = cell;
                        break;
                    }
                }

                Cell newCell = new Cell() { CellReference = cellReference };
                row.InsertBefore(newCell, refCell);

                worksheet.Save();
                return newCell;
            }
        }
    }
}

