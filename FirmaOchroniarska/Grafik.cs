﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirmaOchroniarska
{
    public partial class Grafik : Form
    {
        string id;
        public Grafik(string t_id)
        {
            InitializeComponent();
            this.id = t_id;
            Load += new EventHandler(Load_Grafik);
        }

        private void Load_Grafik(object sender, EventArgs e)
        {
            BazaDanych stanowiskoB = new BazaDanych();
            string zapytanie = "Select Stanowisko from pracownik where ID_prac ='"+this.id+"'";
            string stanowisko = stanowiskoB.zwroc_rekord(zapytanie);
            if(stanowisko=="szef ochrony")
            {
                btnDodajGrafik.Visible = true;
                btnEdytujGrafik.Visible = true;
                btnUsunGrafik.Visible = true;
            }
        }

        private void btnPrzyszly_Click(object sender, EventArgs e)
        {
            if (labMiesiac.Text == "Luty 2020")
            {
                this.picGrafik.Image = global::FirmaOchroniarska.Properties.Resources.grafik_marzec;
                labMiesiac.Text = "Marzec 2020";
                btnPrzyszly.Text = "Wróć do aktualnego grafiku";
            }
            else
            {
                this.picGrafik.Image = global::FirmaOchroniarska.Properties.Resources.grafik;
                labMiesiac.Text = "Luty 2020";
                btnPrzyszly.Text = "Zobacz grafik na przyszły miesiac";
            }
        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void labHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Grafik_Load(object sender, EventArgs e)
        {

        }
    }
}
