﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FirmaOchroniarska
{
    public partial class Zadania : Form
    {
        string id_zespolu;
        public Zadania(string id)
        {
            InitializeComponent();
            this.id_zespolu = id;
            BazaDanych nie_mam_pomyslu_na_nazwe = new BazaDanych();
            txtData.Text=nie_mam_pomyslu_na_nazwe.zwroc_rekord("Select data_zadania from zespol where id_zespol='" + this.id_zespolu + "';");
            txtTresc.Text= nie_mam_pomyslu_na_nazwe.zwroc_rekord("Select tresc_zadania from zespol where id_zespol='" + this.id_zespolu + "';");
        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void labHome_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnZapisziWyjdz_Click(object sender, EventArgs e)
        {
            txtData.Text = "";
            txtTresc.Text = "";
            BazaDanych zadania = new BazaDanych();
            zadania.wykonaj_polecenie("Update zespol set tresc_zadania='"+txtTresc.Text+"',data_zadania ='"+txtData.Text+"' where id_zespol='"+this.id_zespolu+"';");
            MessageBox.Show("Dane zostały zmienione");
        }

        private void btnZapisz_Click(object sender, EventArgs e)
        {
            bool dobrze = true;
            if(txtData.Text.Length!=10)
            {
                dobrze = false;
                MessageBox.Show("Data powinna mieć 10 znaków np. 23/05/2020");
            }
            if (txtTresc.Text.Length >1000)
            {
                dobrze = false;
                MessageBox.Show("Treść zadania nie powinna byc dluzsza niz 1000 znaków");
            }
            if (dobrze)
            {
                BazaDanych zadania = new BazaDanych();
                zadania.wykonaj_polecenie("Update zespol set tresc_zadania='" + txtTresc.Text + "',data_zadania ='" + txtData.Text + "' where id_zespol='" + this.id_zespolu + "';");
                MessageBox.Show("Dane zostały zmienione");
            }
        }
    }
}
