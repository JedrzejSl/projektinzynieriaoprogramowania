﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace FirmaOchroniarska
{
    public partial class Home : Form
    {
        public string id;
        public Home(string id_z_bazy)
        {
            InitializeComponent();
            this.id = id_z_bazy;
            Load += new EventHandler(Home_Load);
        }


        private void Home_Load(object sender, System.EventArgs e)
        {
            BazaDanych przywitanko = new BazaDanych();
            string imie = przywitanko.zwroc_rekord("SELECT Imie from pracownik where ID_prac='"+this.id+"'");
            string nazwisko = przywitanko.zwroc_rekord("SELECT Nazwisko from pracownik where ID_prac='" + this.id + "'");
            string stanowisko = przywitanko.zwroc_rekord("SELECT Stanowisko from pracownik where ID_prac='" + this.id + "'");
            lab_witaj.Text = "Witaj " + imie + " " + nazwisko + "\njesteś zalogowany jako\n" + stanowisko;
            if(stanowisko=="ochroniarz")
            {
                btnPracownicy.Visible = false;
            }
        }

        private void labWylog_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void btnZad_Click(object sender, EventArgs e)
        {
            dodaj_zadania zad = new dodaj_zadania();
            zad.ShowDialog();
        }

        private void btnGrafik_Click(object sender, EventArgs e)
        {
            Grafik graf = new Grafik(this.id);
            graf.ShowDialog();
        }

        private void btnPracownicy_Click(object sender, EventArgs e)
        {
            Pracownik prac = new Pracownik();
            prac.ShowDialog();
        }

        private void Home_Load_1(object sender, EventArgs e)
        {

        }

        private void btnZespoly_Click(object sender, EventArgs e)
        {
            BazaDanych pytanie = new BazaDanych();
            string stanowisko = pytanie.zwroc_rekord("SELECT Stanowisko from pracownik where ID_prac='" + this.id + "'");
            if (stanowisko == "ochroniarz")
            {
                zobacz_zespol ochr = new zobacz_zespol(this.id);
                ochr.ShowDialog();
            }
            else
            {
                Zespol zesp = new Zespol(this.id);
                zesp.ShowDialog();
            }
        }

        private void btnNotatka_Click(object sender, EventArgs e)
        {
            Notatka notatka = new Notatka(id);
            notatka.ShowDialog();
        }

        private void btnRaport_Click(object sender, EventArgs e)
        {
            Raport raport = new Raport(this.id);
            raport.ShowDialog();
        }

        private void lab_opcje_Click(object sender, EventArgs e)
        {

        }
    }
}
