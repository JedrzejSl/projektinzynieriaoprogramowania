﻿namespace FirmaOchroniarska
{
    partial class Zadania
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labWylog = new System.Windows.Forms.Label();
            this.labPom = new System.Windows.Forms.Label();
            this.lab_opcje = new System.Windows.Forms.Label();
            this.labHome = new System.Windows.Forms.Label();
            this.btnZapisz = new System.Windows.Forms.Button();
            this.btnZapisziWyjdz = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTresc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labWylog
            // 
            this.labWylog.AutoSize = true;
            this.labWylog.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labWylog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labWylog.Location = new System.Drawing.Point(603, 9);
            this.labWylog.Name = "labWylog";
            this.labWylog.Size = new System.Drawing.Size(64, 16);
            this.labWylog.TabIndex = 52;
            this.labWylog.Text = "Wyloguj";
            this.labWylog.Click += new System.EventHandler(this.labWylog_Click);
            // 
            // labPom
            // 
            this.labPom.AutoSize = true;
            this.labPom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labPom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labPom.Location = new System.Drawing.Point(535, 10);
            this.labPom.Name = "labPom";
            this.labPom.Size = new System.Drawing.Size(56, 16);
            this.labPom.TabIndex = 51;
            this.labPom.Text = "Pomoc";
            // 
            // lab_opcje
            // 
            this.lab_opcje.AutoSize = true;
            this.lab_opcje.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lab_opcje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lab_opcje.Location = new System.Drawing.Point(435, 3);
            this.lab_opcje.Name = "lab_opcje";
            this.lab_opcje.Size = new System.Drawing.Size(94, 32);
            this.lab_opcje.TabIndex = 50;
            this.lab_opcje.Text = "Opcje \r\nUżytkownika";
            // 
            // labHome
            // 
            this.labHome.AutoSize = true;
            this.labHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labHome.Location = new System.Drawing.Point(362, 10);
            this.labHome.Name = "labHome";
            this.labHome.Size = new System.Drawing.Size(67, 16);
            this.labHome.TabIndex = 49;
            this.labHome.Text = "Zespoły\r\n";
            this.labHome.Click += new System.EventHandler(this.labHome_Click);
            // 
            // btnZapisz
            // 
            this.btnZapisz.Location = new System.Drawing.Point(401, 257);
            this.btnZapisz.Name = "btnZapisz";
            this.btnZapisz.Size = new System.Drawing.Size(105, 23);
            this.btnZapisz.TabIndex = 53;
            this.btnZapisz.Text = "Zapisz";
            this.btnZapisz.UseVisualStyleBackColor = true;
            this.btnZapisz.Click += new System.EventHandler(this.btnZapisz_Click);
            // 
            // btnZapisziWyjdz
            // 
            this.btnZapisziWyjdz.Location = new System.Drawing.Point(236, 257);
            this.btnZapisziWyjdz.Name = "btnZapisziWyjdz";
            this.btnZapisziWyjdz.Size = new System.Drawing.Size(105, 23);
            this.btnZapisziWyjdz.TabIndex = 54;
            this.btnZapisziWyjdz.Text = "Wyczyść i zapisz";
            this.btnZapisziWyjdz.UseVisualStyleBackColor = true;
            this.btnZapisziWyjdz.Click += new System.EventHandler(this.btnZapisziWyjdz_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(12, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(244, 31);
            this.label3.TabIndex = 55;
            this.label3.Text = "Przydziel zadania";
            // 
            // txtTresc
            // 
            this.txtTresc.Location = new System.Drawing.Point(12, 93);
            this.txtTresc.Multiline = true;
            this.txtTresc.Name = "txtTresc";
            this.txtTresc.Size = new System.Drawing.Size(429, 129);
            this.txtTresc.TabIndex = 56;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(172, 74);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 57;
            this.label1.Text = "Treść zadania";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(476, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 16);
            this.label2.TabIndex = 58;
            this.label2.Text = "Data wykonania zadania:\r\n";
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(506, 93);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(116, 20);
            this.txtData.TabIndex = 59;
            // 
            // Zadania
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(688, 292);
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtTresc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnZapisziWyjdz);
            this.Controls.Add(this.btnZapisz);
            this.Controls.Add(this.labWylog);
            this.Controls.Add(this.labPom);
            this.Controls.Add(this.lab_opcje);
            this.Controls.Add(this.labHome);
            this.Name = "Zadania";
            this.Text = "Zadania";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labWylog;
        private System.Windows.Forms.Label labPom;
        private System.Windows.Forms.Label lab_opcje;
        private System.Windows.Forms.Label labHome;
        private System.Windows.Forms.Button btnZapisz;
        private System.Windows.Forms.Button btnZapisziWyjdz;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTresc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtData;
    }
}