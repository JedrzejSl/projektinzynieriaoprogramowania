﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace FirmaOchroniarska
{
    public partial class Dodaj_prac : Form
    {
        public Dodaj_prac()
        {
            InitializeComponent();
            Load += new EventHandler(Load_Dodaj_prac);
        }

        private void Load_Dodaj_prac(object sender, EventArgs e)
        {
            label6.Visible = false;
            textBox6.Visible = false;
            label8.Visible = false;

            label10.Visible = false;
  
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool dobrze = true;
            BazaDanych pracownicy = new BazaDanych();
            string str = "";
            string str2 = "";
            string pozycja = "";
            string login_haslo = "INSERT INTO logowanie(login, haslo) VALUES('" + textBox5.Text + "','" + textBox7.Text + "');";

            if (textBox5.Text.Length > 15)
            {
                dobrze = false;
                MessageBox.Show("Login nie może być dłuższy niż 15 znaków");
            }
            if (textBox5.Text.Length < 5)
            {
                dobrze = false;
                MessageBox.Show("Login musi być dłuższy niż 5 znaków");
            }
            if (textBox7.Text.Length > 15)
            {
                dobrze = false;
                MessageBox.Show("Hasło nie może być dłuższe niż 15 znaków");
            }
            if (textBox7.Text.Length < 5)
            {
                dobrze = false;
                MessageBox.Show("Hasło musi być dłuższe niż 5 znaków");
            }
            if (textBox2.Text.Length > 20)
            {
                dobrze = false;
                MessageBox.Show("Imię nie może być dłuższe niż 20 znaków");
            }
            if (textBox2.Text.Length == 0)
            {
                dobrze = false;
                MessageBox.Show("Imię nie może być puste");
            }
            if (textBox3.Text.Length > 50)
            {
                dobrze = false;
                MessageBox.Show("Nazwisko nie może być dłuższe niż 50 znaków");
            }
            if (textBox3.Text.Length == 0)
            {
                dobrze = false;
                MessageBox.Show("Nazwisko nie może być puste");
            }
            if (int.TryParse(textBox4.Text, out int parsted))
            {
                if (textBox4.Text.Length > 3)
                {
                    dobrze = false;
                    MessageBox.Show("Wiek nie może być dłuższy niż 3 znaki");
                }
                if (textBox4.Text.Length == 0)
                {
                    dobrze = false;
                    MessageBox.Show("Nazwisko nie może być puste");
                }
                if (int.Parse(textBox4.Text) < 18)
                {
                    dobrze = false;
                    MessageBox.Show("Nie można zatrudniać dzieci");
                }
            }
            else
            {
                dobrze = false;
                MessageBox.Show("Wiek jest liczba a nie znakami!");
            }
            if (textBox6.Text.Length > 9)
            {
                dobrze = false;
                MessageBox.Show("Informacja dodatkowa(nr legitymacji, numer zmiany, numer telefonu\nnie moze byc dluzsza niz 9 znakow");
            }
            if (textBox6.Text.Length == 0)
            {
                dobrze = false;
                MessageBox.Show("Informacja dodatkowa(nr legitymacji, numer zmiany, numer telefonu\nnie moze byc pusta");
            }


            if (dobrze)
            {
                if (comboBox1.Text == "Ochroniarz" || comboBox1.Text == "Szef ochrony" || comboBox1.Text == "Szef zmiany")
                {
                    if (pracownicy.wykonaj_polecenie(login_haslo))
                    {
                        string maxid = pracownicy.zwroc_rekord("SELECT max(ID_prac) from logowanie;");
                        if (comboBox1.SelectedItem.ToString() == "Ochroniarz")
                        {
                            pozycja = "ochroniarz";
                            str = "INSERT INTO  pracownik(ID_prac, Imie, Nazwisko, Wiek, Stanowisko) VALUES('" + maxid + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + pozycja + "');";
                            str2 = "INSERT INTO ochroniarze(ID_prac, nr_licencji) VALUES('" + maxid + "','" + textBox6.Text + "');";
                        }
                        if (comboBox1.SelectedItem.ToString() == "Szef ochrony")
                        {
                            pozycja = "szef ochrony";
                            str = "INSERT INTO  pracownik(ID_prac, Imie, Nazwisko, Wiek, Stanowisko) VALUES('" + maxid + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + pozycja + "');";
                            str2 = "INSERT INTO szef_ochrony (ID_prac, telefon) VALUES('" + maxid + "','" + textBox6.Text + "');";
                        }
                        if (comboBox1.SelectedItem.ToString() == "Szef zmiany")
                        {
                            pozycja = "szef zmiany";
                            str = "INSERT INTO  pracownik(ID_prac, Imie, Nazwisko, Wiek, Stanowisko) VALUES('" + maxid + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + pozycja + "');";
                            str2 = "INSERT INTO szef_zmiany (ID_prac, numer_zmiany) VALUES('" + maxid + "','" + textBox6.Text + "');";
                        }
                        if (pracownicy.wykonaj_polecenie(str))
                        {
                            pracownicy.wykonaj_polecenie(str2);
                            MessageBox.Show("Dodano pracownika");

                            textBox2.Text = "";
                            textBox3.Text = "";
                            textBox4.Text = "";
                            textBox6.Text = "";
              
                            textBox5.Text = "";
                            textBox7.Text = "";
                            comboBox1.Text = "";
                         
                        }
                        else
                        {
                            MessageBox.Show("Nastapil blad");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Nastapil blad");
                    }
                }
                else
                {
                    MessageBox.Show("Wybierz rodzaj pracownika przed dodaniem");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        { 
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox6.Text = "";
       
            textBox5.Text = "";
            textBox7.Text = "";
            comboBox1.Text = "";
         
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Ochroniarz")
            {
                label8.Visible = true;
           
                label6.Visible = false;
                textBox6.Visible = true;
                label10.Visible = false;
          

            }
            if (comboBox1.SelectedItem.ToString() == "Szef ochrony")
            {
                label6.Visible = true;
                textBox6.Visible = true;
                label8.Visible = false;
         
                label10.Visible = false;
        
            }
            if (comboBox1.SelectedItem.ToString() == "Szef zmiany")
            {
                label10.Visible = true;
  
                label6.Visible = false;
                textBox6.Visible = true;
                label8.Visible = false;
        
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dodaj_prac_Load(object sender, EventArgs e)
        {

        }
    }
}
