﻿namespace FirmaOchroniarska
{
    partial class Raport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.labWylog = new System.Windows.Forms.Label();
            this.labPom = new System.Windows.Forms.Label();
            this.lab_opcje = new System.Windows.Forms.Label();
            this.labHome = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(29, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 31);
            this.label3.TabIndex = 23;
            this.label3.Text = "Raporty";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(35, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(203, 98);
            this.button1.TabIndex = 24;
            this.button1.Text = "Dodaj raport";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // labWylog
            // 
            this.labWylog.AutoSize = true;
            this.labWylog.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labWylog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labWylog.Location = new System.Drawing.Point(473, 9);
            this.labWylog.Name = "labWylog";
            this.labWylog.Size = new System.Drawing.Size(64, 16);
            this.labWylog.TabIndex = 40;
            this.labWylog.Text = "Wyloguj";
            this.labWylog.Click += new System.EventHandler(this.labWylog_Click);
            // 
            // labPom
            // 
            this.labPom.AutoSize = true;
            this.labPom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labPom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labPom.Location = new System.Drawing.Point(405, 10);
            this.labPom.Name = "labPom";
            this.labPom.Size = new System.Drawing.Size(56, 16);
            this.labPom.TabIndex = 39;
            this.labPom.Text = "Pomoc";
            // 
            // lab_opcje
            // 
            this.lab_opcje.AutoSize = true;
            this.lab_opcje.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lab_opcje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lab_opcje.Location = new System.Drawing.Point(305, 3);
            this.lab_opcje.Name = "lab_opcje";
            this.lab_opcje.Size = new System.Drawing.Size(94, 32);
            this.lab_opcje.TabIndex = 38;
            this.lab_opcje.Text = "Opcje \r\nUżytkownika";
            // 
            // labHome
            // 
            this.labHome.AutoSize = true;
            this.labHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labHome.Location = new System.Drawing.Point(237, 3);
            this.labHome.Name = "labHome";
            this.labHome.Size = new System.Drawing.Size(62, 32);
            this.labHome.TabIndex = 37;
            this.labHome.Text = "Strona \r\nGłówna";
            this.labHome.Click += new System.EventHandler(this.labHome_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(272, 128);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(203, 98);
            this.button5.TabIndex = 49;
            this.button5.Text = "Generuj raporty pracowników";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Raport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(555, 293);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.labWylog);
            this.Controls.Add(this.labPom);
            this.Controls.Add(this.lab_opcje);
            this.Controls.Add(this.labHome);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Name = "Raport";
            this.Text = "Raport";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label labWylog;
        private System.Windows.Forms.Label labPom;
        private System.Windows.Forms.Label lab_opcje;
        private System.Windows.Forms.Label labHome;
        private System.Windows.Forms.Button button5;
    }
}