﻿namespace FirmaOchroniarska
{
    partial class Notatka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labWylog = new System.Windows.Forms.Label();
            this.labPom = new System.Windows.Forms.Label();
            this.lab_opcje = new System.Windows.Forms.Label();
            this.labHome = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labWylog
            // 
            this.labWylog.AutoSize = true;
            this.labWylog.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labWylog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labWylog.Location = new System.Drawing.Point(558, 15);
            this.labWylog.Name = "labWylog";
            this.labWylog.Size = new System.Drawing.Size(64, 16);
            this.labWylog.TabIndex = 44;
            this.labWylog.Text = "Wyloguj";
            this.labWylog.Click += new System.EventHandler(this.labWylog_Click);
            // 
            // labPom
            // 
            this.labPom.AutoSize = true;
            this.labPom.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labPom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labPom.Location = new System.Drawing.Point(490, 16);
            this.labPom.Name = "labPom";
            this.labPom.Size = new System.Drawing.Size(56, 16);
            this.labPom.TabIndex = 43;
            this.labPom.Text = "Pomoc";
            // 
            // lab_opcje
            // 
            this.lab_opcje.AutoSize = true;
            this.lab_opcje.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lab_opcje.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lab_opcje.Location = new System.Drawing.Point(390, 9);
            this.lab_opcje.Name = "lab_opcje";
            this.lab_opcje.Size = new System.Drawing.Size(94, 32);
            this.lab_opcje.TabIndex = 42;
            this.lab_opcje.Text = "Opcje \r\nUżytkownika";
            // 
            // labHome
            // 
            this.labHome.AutoSize = true;
            this.labHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labHome.Location = new System.Drawing.Point(322, 9);
            this.labHome.Name = "labHome";
            this.labHome.Size = new System.Drawing.Size(62, 32);
            this.labHome.TabIndex = 41;
            this.labHome.Text = "Strona \r\nGłówna";
            this.labHome.Click += new System.EventHandler(this.labHome_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(27, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(193, 31);
            this.label3.TabIndex = 45;
            this.label3.Text = "Twoje Notatki";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(247, 276);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(137, 34);
            this.button2.TabIndex = 48;
            this.button2.Text = "Zapisz notatke";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(55, 96);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(537, 174);
            this.textBox1.TabIndex = 50;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(278, 75);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 18);
            this.label10.TabIndex = 52;
            this.label10.Text = "Treść notatki";
            this.label10.Visible = false;
            // 
            // Notatka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(640, 322);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.labWylog);
            this.Controls.Add(this.labPom);
            this.Controls.Add(this.lab_opcje);
            this.Controls.Add(this.labHome);
            this.Name = "Notatka";
            this.Text = "Notatki";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labWylog;
        private System.Windows.Forms.Label labPom;
        private System.Windows.Forms.Label lab_opcje;
        private System.Windows.Forms.Label labHome;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
    }
}